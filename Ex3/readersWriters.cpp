#include "readersWriters.h"
#include <fstream>
#include <vector>
#include <iostream>
using namespace std;

readersWriters::readersWriters(std::string fileName) :_fileName(fileName), _readersNumber(0), _writersNumber(0)
{
	_locker = std::unique_lock<std::mutex>(_mu, std::defer_lock);
}

void readersWriters::readLock()
{
	_locker.lock();
	while (_writersNumber>0)
	{
		cout << "read wait" << endl;
		_condR.wait(_locker);
	}
	_readersNumber++;
	_locker.unlock();
}
void readersWriters::writeLock()
{
	_locker.lock();
	while (_writersNumber>0 || _readersNumber>0)
	{
		cout << "write wait" << endl;
		_condW.wait(_locker);
	}
	_writersNumber++;
	_locker.unlock();
}
void readersWriters::readUnlock()
{
	_locker.lock();
	_readersNumber--;
	if (_readersNumber == 0)
		_condW.notify_one();
	_locker.unlock();
}
void readersWriters::writeUnlock()
{
	_locker.lock();
	_writersNumber--;
	if (_writersNumber == 0)
	{
		_condR.notify_all();
		_condW.notify_all();
	}
	_locker.unlock();
}

string readersWriters::readLine(int lineNumber)
{
	int lineCounter;
	ifstream file(_fileName);
	string line;
	readLock();
	for (lineCounter = 0; lineCounter < lineNumber && getline(file, line); lineCounter++){}
	readUnlock();

	if (lineCounter < lineNumber)
	{
		return "the requested line doesn't exist";
	}
	return line;
}


void readersWriters::WriteLine(int lineNumber,string newLine)
{
	if (lineNumber < 1)
	{
		cout << "invalid lineNumber" << endl;
	}
	int nextLineNumber;
	fstream file(_fileName, ios::in | ios::out);
	string line;
	readLock();
	for (nextLineNumber = 1; nextLineNumber < lineNumber && getline(file, line); nextLineNumber++){}
	readUnlock();


	if (nextLineNumber != lineNumber )
	{
		cout << "the line you are trying to write to is invalid" << endl;
	}
	else
	{
		writeLock();
		int length = file.tellg();
		if (file.seekp(length))
		{
			file << newLine;
		}
		//we get here only if the line we supposed to write to, doesn't exist in the file
		//and the lineNumber is the next line in the file
		else 
		{
			file.close();
			file.open(_fileName,ios::app);
			file <<endl<<newLine;
		}
		writeUnlock();
			
	}
	

}