From Wikipedia - C++:

C++ is a general-purpose programming language. 
It has imperative, object-oriented and generic programming features
, while also providing facilities for low-level memory manipulation.

It was designed with a bias toward system programming and embedded, 
resource-constrained and large systems, with performance,
efficiency and flexibility of use as its design highlights.
C++ has also been found useful in many other contexts,
with key strengths being software infrastructure and
resource-constrained applications,
ncluding desktop applications,
servers (e.g. e-commerce, web search or SQL servers), 
and performance-critical applications (e.g. telephone switches or space probes).