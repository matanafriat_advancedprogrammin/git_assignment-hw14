#include "threads.h"
#include <thread>
#include <mutex>

mutex mtx;

void I_Love_Threads()
{
	cout << "I Love Threads" << endl;
}

void call_I_Love_Threads()
{
	thread print (I_Love_Threads);

	print.join();
}

void printVector(vector<int> primes)
{
	for (int i = 0; i < primes.size(); ++i)
	{
		cout << primes.at(i) << endl;
	}
}

void getPrimes(int begin, int end, vector<int>& primes)
{
	bool isPrime = true;
	for (int i = begin; i < end; i++) //check for primes 
	{
		for (int j = 2; j < i && isPrime; j++)
		{
			if (i % j == 0)
			{
				isPrime = false;
			}
		}
		if (isPrime && i > 2)
		{
			primes.push_back(i);
		}
		isPrime = true;
	}
}

vector<int> callGetPrimes(int begin, int end)
{
	vector<int> primes;
	thread getPrimes(getPrimes, begin, end, ref(primes));
	const clock_t begin_time = clock();
	getPrimes.join();
	cout << "It took " << (float)(clock() - begin_time)/CLOCKS_PER_SEC << " seconds" << endl;
	return primes;
}

void writePrimesToFile(int begin, int end, ofstream& file)
{
	mtx.lock();
	bool isPrime = true;
	for (int i = begin; i < end; i++) //check for primes 
	{
		for (int j = 2; j < i && isPrime; j++)
		{
			if (i % j == 0)
			{
				isPrime = false;
			}
		}
		if (isPrime && i > 2)
		{
			file << i << endl;
		}
		isPrime = true;
	}
	mtx.unlock();
}

void callWritePrimesMultipleThreads(int begin, int end, string filePath, int N)
{
	ofstream file;
	file.open(filePath);
	const clock_t begin_time = clock();
	for (int i = 0; i < N; i++)
	{
		thread t(writePrimesToFile, begin, end, ref(file));
		t.join();
	}
	cout << "It took " << (float)(clock() - begin_time) / CLOCKS_PER_SEC << " seconds" << endl;

}